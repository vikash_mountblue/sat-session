# _Project Analysis Report on Full Text Search_

#### Elasticsearch

- Elasticsearch is a search engine built on apache lucene.
- Its is real time update
- It is able to achieve fast search responses because, instead of searching the text directly, it searches an index.
- It makes cluster containig nodes. Growing from small cluster to large cluster its automatic done the cluster of nodes.
- Many settings exposed by ElasticSearch can be changed on the live cluster for which the ElasticSearch nodes don’t require a restart.
- Its update

#### Solr

- Solr is a search engine built on apache lucene.
- In Solr, the configuration of all components is defined in the solrconfig.xml file and after each change, restart, or reload of Solr node, it is needed.
- Its is not update Properly

#### Lucene

- Lucene is an inverted full-text index.
- It takes all the documents, splits them into words, and then builds an index for each word. Since the index is an exact string-match, unordered, it can be extremely fast.
- Lucene does not have to optimize for transaction processing. When you add a document, it need not ensure that queries see it instantly. And it need not optimize for updates to existing documents.

## Conclusion

Solr is search server for creating standard search applications, no massive indexing and no real time updates are required, but on the other hand Elasticsearch takes it to the next level with an architecture aimed at building modern real-time search applications.

## Links

[Elasticsearch](https://medium.com/@AIMDekTech/what-is-elasticsearch-why-elasticsearch-advantages-of-elasticsearch-47b81b549f4d)

[Solr](https://en.wikipedia.org/wiki/Apache_Solr)

[Lucene](https://stackoverflow.com/questions/2705670/how-does-lucene-work)
